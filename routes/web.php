<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();\

Route::prefix('admin')->middleware('checkAdmin')->group(function () {
	Route::namespace('Admin')->group(function () {
		Route::get('/', 'AdminController@index')->name('adminIndex');

		Route::get('/photos', 'PhotosController@index')->name('photosIndex');
		Route::get('/photos/create', 'PhotosController@create')->name('createPhoto');
		Route::post('/photos', 'PhotosController@store')->name('storePhoto');
		Route::get('/photos/show/{id}', 'PhotosController@show')->name('showPhoto');
		Route::get('/photos/edit/{id}', 'PhotosController@edit')->name('editPhoto');
		Route::patch('/photos/update/{id}', 'PhotosController@update')->name('updatePhoto');
		Route::delete('/photos/{id}', 'PhotosController@destroy')->name('deletePhoto');

		Route::get('/goods', 'GoodsController@index')->name('goodsIndex');
		Route::post('/goods/search', 'GoodsController@index')->name('searchGoods');
		Route::get('/goods/create', 'GoodsController@edit')->name('createGood');
		Route::post('/goods', 'GoodsController@store')->name('storeGood');
		Route::get('/goods/show/{id}', 'GoodsController@show')->name('showGood');
		Route::get('/goods/edit/{id}', 'GoodsController@edit')->name('editGood');
		Route::patch('/goods/update/{id}', 'GoodsController@update')->name('updateGood');
		Route::delete('/goods/{id}', 'GoodsController@destroy')->name('deleteGood');

		Route::get('/pickup_points', 'PickupPointsController@index')->name('pickupPointsIndex');
		Route::get('/pickup_points/create', 'PickupPointsController@create')->name('createPickupPoint');
		Route::post('/pickup_points', 'PickupPointsController@store')->name('storePickupPoint');
		Route::get('/pickup_points/show/{id}', 'PickupPointsController@show')->name('showPickupPoint');
		Route::get('/pickup_points/edit/{id}', 'PickupPointsController@edit')->name('editPickupPoint');
		Route::patch('/pickup_points/update/{id}', 'PickupPointsController@update')->name('updatePickupPoint');
		Route::delete('/pickup_points/{id}', 'PickupPointsController@destroy')->name('deletePickupPoint');

		Route::get('/comments', 'CommentsController@index')->name('commentsIndex');
		Route::get('/comments/create', 'CommentsController@create')->name('createComment');
		Route::post('/comments', 'CommentsController@store')->name('storeComment');
		Route::get('/comments/show/{id}', 'CommentsController@show')->name('showComment');
		Route::get('/comments/edit/{id}', 'CommentsController@edit')->name('editComment');
		Route::patch('/comments/update/{id}', 'CommentsController@update')->name('updateComment');
		Route::delete('/comments/{id}', 'CommentsController@destroy')->name('deleteComment');

		Route::get('/categories', 'CategoriesController@index')->name('categoriesIndex');
		Route::get('/categories/create', 'CategoriesController@create')->name('createCategories');
		Route::post('/categories', 'CategoriesController@store')->name('storeCategories');
		Route::get('/categories/show/{id}', 'CategoriesController@show')->name('showCategories');
		Route::get('/categories/edit/{id}', 'CategoriesController@edit')->name('editCategories');
		Route::patch('/categories/update/{id}', 'CategoriesController@update')->name('updateCategories');
		Route::delete('/categories/{id}', 'CategoriesController@destroy')->name('deleteCategories');

		Route::get('/menus', 'MenusController@index')->name('menusIndex');
		Route::get('/menus/create', 'MenusController@create')->name('createMenuItem');
		Route::post('/menus', 'MenusController@store')->name('storeMenuItem');
		Route::get('/menus/show/{id}', 'MenusController@show')->name('showMenuItem');
		Route::get('/menus/edit/{id}', 'MenusController@edit')->name('editMenuItem');
		Route::patch('/menus/update/{id}', 'MenusController@update')->name('updateMenuItem');
		Route::delete('/menus/{id}', 'MenusController@destroy')->name('deleteMenuItem');
	});
});
Route::post('/comments', 'Admin\CommentsController@store')->name('storeComment');
Route::get('/goods', 'GoodsController@index')->name('goodsIndexGuest');
Route::get('/goods/{id}', 'GoodsController@show')->name('showGoodGuest');
Route::get('/orders/{id}', 'OrdersController@show')->name('orderHistoryPage');
Route::middleware('PrepareCart')->group(function () {
	Route::get('/order', 'OrdersController@create')->name('orderPage');
	Route::post('/order', 'OrdersController@store')->name('storeOrder');
	Route::get( '/cart', 'CartController@show' )->name( 'showCart' );
	Route::post( '/cart', 'CartController@addProduct' )->name( 'addProduct' );
	Route::delete( '/cart', 'CartController@removeProduct' )->name( 'removeProduct' );
});

// категории
Route::get('/categories', 'CategoriesController@index')->name('categoiesIndexGuest');
Route::get('/category/{id}', 'CategoriesController@show')->name('showCategoryGuest');
Route::get('/map', 'CategoriesController@map')->name('categoiesMapGuest');