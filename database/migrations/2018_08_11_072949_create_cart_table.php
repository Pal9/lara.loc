<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCartTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cart', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('userId');
            $table->text('goods')->nullable();
            $table->timestamps();

	        $table->foreign('userId')->references('id')->on('users')
	              ->onDelete('cascade');
        });

	    Schema::table('orders', function (Blueprint $table) {
		    $table->text('goods');
	    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cart');
    }
}
