<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGoodPhotosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('good_photos', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('good_id');
            $table->unsignedInteger('photo_id');
        });

	    Schema::table('good_photos', function (Blueprint $table) {
		    $table->foreign('photo_id')->references('id')->on('photos')
		                                                 ->onDelete('cascade');
	    });

	    Schema::table('good_photos', function (Blueprint $table) {
		    $table->foreign('good_id')->references('id')->on('goods')
		                                                ->onDelete('cascade');
	    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('good_photos');
    }
}
