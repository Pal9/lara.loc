<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGoodsOnPickupPointsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('goods_on_pickup_points', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('goods_id');
            $table->unsignedInteger('pickup_points_id');

	        $table->foreign('pickup_points_id')->references('id')->on('pickup_points')
	              ->onDelete('NO ACTION')->onUpdate('NO ACTION');

	        $table->foreign('goods_id')->references('id')->on('goods')
	              ->onDelete('NO ACTION')->onUpdate('NO ACTION');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('goods_on_pickup_points');
    }
}
