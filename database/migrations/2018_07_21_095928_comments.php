<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Comments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

	    Schema::create('comments', function (Blueprint $table) {
		    $table->increments('id');
		    $table->text('text');
		    $table->unsignedInteger('good_id');
	    });

	    Schema::table('comments', function (Blueprint $table) {
		    $table->foreign('good_id')->references('id')->on('goods');
	    });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
