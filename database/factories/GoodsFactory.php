<?php

use Faker\Generator as Faker;

$factory->define(\App\Goods::class, function (Faker $faker) {
    return [
        'name' => $faker->firstName(),
        'short_description' => $faker->name,
        'description' => $faker->word,
        'icon' => $faker->word,
    ];
});
