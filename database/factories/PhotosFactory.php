<?php

use App\models\admin\Photos;
use Faker\Generator as Faker;

$factory->define(Photos::class, function (Faker $faker) {
    return [
	    'name' => $faker->name,
	    'alt' => $faker->name,
	    'title' => $faker->name,
	    'path' => $faker->name,
    ];
});
