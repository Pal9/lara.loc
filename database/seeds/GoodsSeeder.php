<?php

use Illuminate\Database\Seeder;

class GoodsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	for ($i = 0; $i < 100; $i++) {
		    DB::table('goods')->insert([
			    'name' => 'some name' . rand(1, 100),
			    'short_description' => str_random(100),
			    'description' => 'description ' . rand(1, 100),
			    'icon' => 'path ' . rand(1, 100),
		    ]);
	    }
    }
}
