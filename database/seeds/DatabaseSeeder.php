<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
         $this->call(MenuSeeder::class);
//         $this->call(PhotosSeeder::class);
//         $this->call(GoodsSeeder::class);
//         $this->call(PostGoodsSeeder::class);
    }
}
