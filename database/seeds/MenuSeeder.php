<?php

use Illuminate\Database\Seeder;

class MenuSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	// Заполняем админ-меню
	    DB::table('menus')->insert([
		    'name' => 'Категории',
		    'link' => '/admin/categories',
		    'type' => 1,
		    'icon' => 'box',
	    ]);
	    DB::table('menus')->insert([
		    'name' => 'Товары',
		    'link' => '/admin/goods',
		    'type' => 1,
		    'icon' => 'shopping-cart',
	    ]);
	    DB::table('menus')->insert([
		    'name' => 'Фото',
		    'link' => '/admin/photos',
		    'type' => 1,
		    'icon' => 'file',
	    ]);
	    DB::table('menus')->insert([
		    'name' => 'Комментарии',
		    'link' => '/admin/comments',
		    'type' => 1,
		    'icon' => 'layers',
	    ]);
	    DB::table('menus')->insert([
		    'name' => 'Адреса',
		    'link' => '/admin/pickup_points',
		    'type' => 1,
		    'icon' => 'home',
	    ]);
	    DB::table('menus')->insert([
		    'name' => 'Меню',
		    'link' => '/admin/menus',
		    'type' => 1,
		    'icon' => 'list',
	    ]);
	    DB::table('menus')->insert([
		    'name' => 'Перейти на сайт',
		    'link' => '/',
		    'type' => 1,
		    'icon' => 'link',
	    ]);
    }
}
