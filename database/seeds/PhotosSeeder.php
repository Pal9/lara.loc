<?php

use App\models\admin\Photos;
use Illuminate\Database\Seeder;

class PhotosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
//	    DB::table('photos')->insert([
//		    'name' => 'some name' . rand(1, 100),
//		    'alt' => str_random(100),
//		    'title' => 'description ' . rand(1, 100),
//		    'path' => 'path ' . rand(1, 100),
//	    ]);
	    factory(Photos::class, 50)->create()->each(function ($u) {
		    $u->photos()->save(factory(Photos::class)->make());
	    });
    }
}
