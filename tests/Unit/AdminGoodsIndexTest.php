<?php
/**
 * Created by PhpStorm.
 * User: Андрусь
 * Date: 28.07.18
 * Time: 14:40
 */

namespace Tests\Unit;

use Tests\TestCase;

class AdminGoodsIndexTest extends TestCase {

	public function testIndexTest()
	{
		$this->get('/admin/goods')->assertStatus(302);
	}
}
