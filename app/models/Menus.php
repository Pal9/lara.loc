<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class Menus extends Model
{

	protected  $table = 'menus';
	protected $fillable = ['name', 'link', 'type', 'icon']; // разрешаем массовое заполнение свойств через Model::create
	public $timestamps = false; // зафолсить, если в таблице не созданы created at и updated at

    public static function getItems(int $type = 1) {
    	return self::all()->where('type', '=', $type);
    }
}
