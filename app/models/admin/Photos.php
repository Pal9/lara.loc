<?php

namespace App\models\admin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Photos extends Model
{
	public $timestamps = false; // зафолсить, если в таблице не созданы created at и updated at

	protected $fillable = ['name', 'alt', 'title', 'path']; // разрешаем массовое заполнение свойств через Model::create
	protected $guarded = []; // запрещаем массовое заполнение свойств через Model::create

	public function getItems() {
		return DB::table('photos')->get();
	}
}
