<?php

namespace App\models\admin;

use Illuminate\Database\Eloquent\Model;

class PickupPoints extends Model
{
	public $timestamps = false; // зафолсить, если в таблице не созданы created at и updated at

	protected $fillable = ['name', 'address']; // разрешаем массовое заполнение свойств через Model::create
	protected $guarded = []; // запрещаем массовое заполнение свойств через Model::create

	public function goods() {
		return $this->belongsToMany(Goods::class, 'goods_on_pickup_points', 'pickup_points_id', 'goods_id');
	}
}
