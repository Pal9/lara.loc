<?php

namespace App\models\admin;

use Illuminate\Database\Eloquent\Model;

class Comments extends Model
{
	protected $fillable = ['good_id', 'text']; // разрешаем массовое заполнение свойств через Model::create

	public function goods() {
		return $this->belongsTo(Goods::class, 'good_id', 'id');
	}
}
