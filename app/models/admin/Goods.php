<?php

namespace App\models\admin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Goods extends Model
{

	public $timestamps = false; // зафолсить, если в таблице не созданы created at и updated at

	protected $fillable = ['name', 'short_description', 'description', 'icon', 'parentCat', 'price']; // разрешаем массовое заполнение свойств через Model::create
	protected $guarded = []; // запрещаем массовое заполнение свойств через Model::create

	/**
	 * Хлебные крошки для товара
	 *
	 * @param Goods $item
	 *
	 * @return array
	 */
	public static function getBreadcrumbs(Goods $item) : array {

		$breadcrumbs = [];

		$breadcrumbs[$item->name] = '';
		while ($item = $item->parentCategory) {
			$breadcrumbs[$item->title] = route('showCategoryGuest', [
				'id' => $item->id
			]);
		}

		$breadcrumbs['Каталог'] = '/categories';
		$breadcrumbs['Главная'] = '/';

		$breadcrumbs = array_reverse($breadcrumbs);

		return $breadcrumbs;
	}

	public function getItems() {
		return DB::table('goods')->get();
	}

	public function comments() {
		return $this->hasMany(Comments::class, 'good_id');
	}

	public function addComment($text)
	{
		$this->comments()->create(['text' => $text]);
	}

	public function photos() {
		return $this->belongsToMany(Photos::class, 'good_photos', 'good_id', 'photo_id');
	}

	public function pickup_points() {
		return $this->belongsToMany(PickupPoints::class, 'goods_on_pickup_points', 'goods_id', 'pickup_points_id');
	}

	/**
	 * Связь с родителькой категорией
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\HasOne
	 */
	public function parentCategory() {
		return $this->hasOne(Categories::class, 'id', 'parentCat');
	}
}
