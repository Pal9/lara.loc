<?php

namespace App\models\admin;

use Illuminate\Database\Eloquent\Model;

class Categories extends Model
{
	protected $fillable = ['title', 'parentCat']; // разрешаем массовое заполнение свойств через Model::create

	/**
	 * Хлебные крошки для категории
	 *
	 * @param Categories|null $category
	 *
	 * @return array
	 */
	public static function getBreadcrumbs(Categories $category = NULL) : array {

		$breadcrumbs = [];

		if (empty($category)) {
			$breadcrumbs['Каталог'] = '';
		} else {
			$breadcrumbs[$category->title] = '';
			while ($category = $category->parentCategory) {
				$breadcrumbs[$category->title] = route('showCategoryGuest', [
					'id' => $category->id
				]);
			}
			$breadcrumbs['Каталог'] = '/categories';
		}
		$breadcrumbs['Главная'] = '/';

		$breadcrumbs = array_reverse($breadcrumbs);

		return $breadcrumbs;
	}

	/**
	 * Связь с дочерними категориями
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function categories() {
		return $this->hasMany(self::class, 'parentCat');
	}

	/**
	 * Связь с дочерними товарами
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function goods() {
		return $this->hasMany(Goods::class, 'parentCat');
	}

	/**
	 * Связь с родителькой категорией
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\HasOne
	 */
	public function parentCategory() {
		return $this->hasOne(self::class, 'id', 'parentCat');
	}
}
