<?php

namespace App\models;

use App\models\admin\Goods;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class CartGuest extends Model implements CartInterface
{

	/**
	 * Получить массив товаров в корзине
	 *
	 * @param Request $request
	 *
	 * @return array
	 */
	private function getCart(Request $request) : array {
		return empty($request->cookie('cart')) ? [] : unserialize($request->cookie('cart'));
	}

	/**
	 * Получаем список айди товаров без дополнительной информации
	 *
	 * @param Request $request
	 *
	 * @return array
	 */
	public function getProductList(Request $request) : array {

		return $this->getCart($request);
	}

	/**
	 * Добавление товара в корзину
	 *
	 * @param Request $request
	 *
	 * @return array
	 */
	public function addToCart(Request $request) : array {
		$prodId = $request->post('prodId');

		$cart = $this->getCart($request);

		$cart[] = $prodId;

		return $cart;
	}

	/**
	 * Удаление товара из корзины
	 *
	 * @param int $id
	 *
	 * @return array
	 */
	public function removeFromCart( int $id ) : array {

		$needToRemove = true;

		$prodsInCart = $this->getCart(\request());

		$products = [];

		foreach ($prodsInCart as $prodId) {
			if ($prodId == $id && $needToRemove) {
				$needToRemove = false; // т.к. записей с одинаковым товаром м.б. несколько, удаляем только одну из них
				continue;
			}
			$products[] = $prodId;
		}

		return $products;
	}

	/**
	 * Выборка товаров в корзине
	 *
	 * @param Request $request
	 *
	 * @return array
	 */
	public function getProducts(Request $request) : array {

		$cart = $this->getProductList($request);
		$products = [];

		foreach ($cart as $prodId) {
			$products[] = Goods::find($prodId);
		}

		return $products;
	}
}
