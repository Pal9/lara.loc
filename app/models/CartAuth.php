<?php

namespace App\models;

use App\models\admin\Goods;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CartAuth extends Model implements CartInterface
{
	protected $table = 'cart';
	protected $fillable = ['userId', 'goods']; // разрешаем массовое заполнение свойств через Model::create

	/**
	 * Получить объект корзины
	 *
	 * @return self
	 */
	private function getCart() : CartInterface {
		return self::where('userId', '=', Auth::id())->first() ??
		       self::create([
			       'userId' => Auth::id(),
		       ]);
	}

	/**
	 * Получаем список айди товаров без дополнительной информации
	 *
	 * @param Request $request
	 *
	 * @return array
	 */
	public function getProductList(Request $request) : array {

		$cart = $this->getCart();

		return explode(',', $cart->goods);
	}

	/**
	 * Добавление товара в корзину
	 *
	 * @param Request $request
	 *
	 * @return array
	 */
	public function addToCart(Request $request) : array {

		$prodId = $request->post('prodId'); // id добавляемого товара

		$cart = $this->getCart();

		$prodsInCart = $cart->goods ? explode(',', $cart->goods) : [];
		$prodsInCart[] = $prodId;
		$cart->goods = implode(',', $prodsInCart);
		$cart->save();

		return $prodsInCart;
	}

	/**
	 * Удаление товара из корзины
	 *
	 * @param int $id
	 *
	 * @return array
	 */
	public function removeFromCart(int $id) : array {

		$needToRemove = true;

		$cart = $this->getCart();

		$prodsInCart = explode(',', $cart->goods);

		$products = [];

		foreach ($prodsInCart as $prodId) {
			if ($prodId == $id && $needToRemove) {
				$needToRemove = false; // т.к. записей с одинаковым товаром м.б. несколько, удаляем только одну из них
				continue;
			}
			$products[] = $prodId;
		}

		$cart->goods = implode(',', $products);
		$cart->save();

		return $products;
	}

	/**
	 * Просмотр товаров в корзине
	 *
	 * @param Request $request
	 *
	 * @return array
	 */
	public function getProducts(Request $request) : array {

		$prodsInCart = $this->getProductList($request);

		$products = [];

		foreach ($prodsInCart as $prodId) {
			if ($prodId) {
				$products[] = Goods::find($prodId);
			}
		}

		return $products;
	}
}
