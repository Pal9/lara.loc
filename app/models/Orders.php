<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class Orders extends Model
{
	protected $fillable = ['name', 'address', 'email', 'goods']; // разрешаем массовое заполнение свойств через Model::create
	protected $guarded = []; // запрещаем массовое заполнение свойств через Model::create


}
