<?php

namespace App\models;

use Illuminate\Http\Request;

interface CartInterface
{
	public function addToCart(Request $request) : array;
	public function getProducts(Request $request) : array;
	public function removeFromCart(int $id) : array;
}
