<?php
/**
 * Created by PhpStorm.
 * User: Андрусь
 * Date: 22.07.18
 * Time: 16:05
 */

namespace App\Http\Controllers\Admin;


use App\Http\Controllers\Controller;

class AdminController extends Controller {

	public function index() {
		return view('admin.index');
	}

}