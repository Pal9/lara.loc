<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\MenusRequest;
use App\models\Menus;

class MenusController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
	    return view('admin.menus.index', [
		    'menus' => Menus::paginate(10),
	    ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
	    return view('admin.menus.create', [
		    'menuItem' => new Menus(),
	    ]);
    }

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param MenusRequest $request
	 *
	 * @return \Illuminate\Http\Response
	 */
    public function store(MenusRequest $request)
    {
	    Menus::create($request->all());

	    return redirect('/admin/menus');
    }

	/**
	 * Display the specified resource.
	 *
	 * @param int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
    public function show(int $id)
    {
	    return view('admin.menus.show',
		    [
			    'menuItem' => Menus::find($id),
		    ]
	    );
    }

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
    public function edit(int $id)
    {
	    return view('admin.menus.create',
		    [
			    'menuItem' => Menus::find($id),
		    ]
	    );
    }

	/**
	 * Update the specified resource in storage.
	 *
	 * @param MenusRequest $request
	 * @param int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
    public function update(MenusRequest $request, int $id)
    {
	    Menus::find($id)->update($request->all());

	    return redirect('/admin/menus');
    }

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
    public function destroy(int $id)
    {
	    Menus::find($id)->delete();

	    return redirect(route('menusIndex'));
    }
}
