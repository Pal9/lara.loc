<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\GoodsRequest;
use App\models\admin\Categories;
use App\models\admin\Goods;
use App\models\admin\Photos;
use App\models\admin\PickupPoints;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

class GoodsController extends Controller
{

	/**
	 * Display a listing of the resource.
	 *
	 * @param Request $request
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index(Request $request)
	{
//		dd($request->myVeryCustomField);
		$goods = new Goods();
		$searchBy = [
			'by-string' => '',
			'price-from' => '',
			'price-to' => '',
		];

		if ($request->isMethod('post')) {
			// пришёл запрос с поисковой формы
			if (is_string($_POST['by-string']) && !empty($_POST['by-string'])) {
				$searchBy['by-string'] = $_POST['by-string'];
				$goods = $goods->where(function($q) use ($searchBy){
					$q->where('name', 'like', '%' . $searchBy['by-string'] . '%')
					  ->orWhere('short_description', 'like', '%' . $searchBy['by-string'] . '%')
					  ->orWhere('description', 'like', '%' . $searchBy['by-string'] . '%');
				});
			}
			if (is_numeric($_POST['price-from'])) {
				$searchBy['price-from'] = $_POST['price-from'];
				$goods = $goods->where('price', '>=', $searchBy['price-from']);
			}
			if (is_numeric($_POST['price-to'])) {
				$searchBy['price-to'] = $_POST['price-to'];
				$goods = $goods->where('price', '<=', $searchBy['price-to']);
			}
		}

		return view('admin/goods/index', [
			'goods' => $goods->paginate(10),
			'count' => $goods->count(),
			'sum' => $goods->sum('price'),
			'searchBy' => $searchBy,
		]);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \App\Http\Requests\GoodsRequest  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(GoodsRequest $request)
	{
		if ($request->parentCat == -1) {
			$request->request->set('parentCat', null);
		}

		$file = $request->file('image');

		if (!empty($file)) {
			$result = $file->move(public_path() . DIRECTORY_SEPARATOR . 'uploads', 'good_' . (Goods::max('id') + 1) . '.' . $file->extension());

			$request->request->add(['icon' => '/uploads/' . $result->getFilename()]);
		}

		$good = Goods::create($request->all());
		$good->pickup_points()->attach($request->pickup_points);

		return redirect('/admin/goods');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
		$item = Goods::find($id);

		return view('admin.goods.show',
			[
				'good' => $item,
				'comments' => $item->comments->sortByDesc('created_at'),
			]
		);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit(int $id = null)
	{
		$good = $id ? Goods::find($id) : new Goods();
		return view('admin.goods.create',
			[
				'good' => $good,
				'categories' => Categories::all(),
				'pickup_points' => PickupPoints::all(),
				'checked_pickup_points' => $good->pickup_points->pluck('id')->toArray(),
				'photos' => Photos::all(),
				'checked_photos' =>  $good->photos->pluck('id')->toArray(),
			]
		);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \App\Http\Requests\GoodsRequest  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(GoodsRequest $request, int $id)
	{

		if ($request->parentCat == -1) {
			$request->request->set('parentCat', null);
		}

		$file = $request->file('image');
		if (!empty($file)) {
			$result = $file->move(
				public_path() . DIRECTORY_SEPARATOR . 'uploads', 'good_' . $id . '.' . $file->extension()
			);
			$request->request->add(['icon' => '/uploads/' . $result->getFilename()]);
		}

		$good = Goods::find($id);
		$good->pickup_points()->detach(PickupPoints::all('id'));
		$good->pickup_points()->attach($request->pickup_points);
		$good->photos()->detach(Photos::all('id'));
		if (is_array($request->photos)) {
			$good->photos()->attach(array_diff($request->photos, [-1]));
		}

		Goods::find($id)->update($request->all());

		return redirect('/admin/goods');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
		Goods::find($id)->delete();

		return redirect('/admin/goods');
	}
}
