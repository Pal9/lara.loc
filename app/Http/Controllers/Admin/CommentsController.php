<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\CommentsRequest;
use App\Mail\Mailer;
use App\models\admin\Goods;
use App\models\admin\Comments;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class CommentsController extends Controller
{
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		return view('admin/comments/index', [
			'comments' => Comments::all()
		]);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		return view('admin/comments/create',
			[
				'comment' =>  new Comments(),
			]);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \App\Http\Requests\CommentsRequest $request
	 * @param int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function store(CommentsRequest $request)
	{
		$item = Goods::find($request->good_id);

		$item->addComment($request->text);

		$user = \Auth::user();

		if ($user !== null && !empty($user->email)) {
			try {
				Mail::to($user)->send(new Mailer($user));
			} catch (\Exception $exception) {
				Log::notice('При отправке подтверждения добавления комментария на адрес ' . $user->email . ' произошла ошибка: ' . $exception->getMessage());
			}
		}

		session()->flash('message', 'Комментарий успешно добавлен');

		return back();
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
		$item = Comments::find($id);

		return view('admin.comments.show',
			[
				'comment' => $item,
			]
		);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{
		return view('admin.comments.create',
			[
				'comment' => Comments::find($id),
			]
		);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \App\Http\Requests\CommentsRequest  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(CommentsRequest $request, $id)
	{
		Comments::find($id)->update($request->all());

		return redirect('/admin/comments');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
		Comments::find($id)->delete();

		return redirect('/admin/comments');
	}
}
