<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\models\admin\PickupPoints;
use Illuminate\Http\Request;

class PickupPointsController extends Controller
{

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		return view('admin/pickup_points/index', [
			'pickup_points' => PickupPoints::all()
		]);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		return view('admin/pickup_points/create',
			[
				'pickup_point' =>  new PickupPoints(),
			]);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \App\Http\Requests\GoodsRequest  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		PickupPoints::create($request->all());

		return redirect('/admin/pickup_points');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
		$item = PickupPoints::find($id);

		return view('admin.pickup_points.show',
			[
				'pickup_point' => $item,
			]
		);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{
		var_dump(1);

		return view('admin.pickup_points.create',
			[
				'pickup_point' => PickupPoints::find($id),
			]
		);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \App\Http\Requests\GoodsRequest  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id)
	{
		PickupPoints::find($id)->update($request->all());

		return redirect('/admin/pickup_points');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
		PickupPoints::find($id)->delete();

		return redirect('/admin/pickup_points');
	}
}
