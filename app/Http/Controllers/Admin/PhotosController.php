<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\PhotosRequest;
use App\models\admin\Photos;

class PhotosController extends Controller
{
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		return view('admin/photos/index', [
			'photos' => Photos::paginate(10),
		]);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		return view('admin/photos/create',
			[
				'photo' => new Photos(),
			]);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \App\Http\Requests\PhotosRequest  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(PhotosRequest $request)
	{
		$file = $request->file('image');
		$result = $file->move(public_path() . DIRECTORY_SEPARATOR . 'uploads', $file->getClientOriginalName());
		$request->request->add(['path' => '/uploads/' . $result->getFilename()]);
		Photos::create($request->all());

		return redirect('/admin/photos');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
		$photo = Photos::find($id);

		return view('admin.photos.show',
			[
				'photo' => $photo,
			]
		);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{
		return view('admin.photos.create',
			[
				'photo' => Photos::find($id),
			]
		);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \App\Http\Requests\PhotosRequest  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(PhotosRequest $request, $id)
	{
		$file = $request->file('image');
		if (!empty($file)) {
			$result = $file->move(public_path() . DIRECTORY_SEPARATOR . 'uploads', $file->getClientOriginalName());

			$request->request->add(['path' => '/uploads/' . $result->getFilename()]);
		}
		Photos::find($id)->update($request->all());

		return redirect('/admin/photos');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
		Photos::find($id)->delete();

		return redirect('/admin/photos');
	}
}
