<?php

namespace App\Http\Controllers\admin;

use App\models\admin\Categories;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CategoriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
	    return view('admin/categories/index', [
			    'categories' => Categories::paginate(15)
		    ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
	    return view('admin/categories/create', [
			    'category' =>  new Categories(),
			    'categories' => Categories::all(),
		    ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
	    if ($request->parentCat == -1) {
		    $request->request->set('parentCat', null);
	    }

	    Categories::create($request->all());

	    return redirect('/admin/categories');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
	    $item = Categories::find($id);

	    return view('admin.categories.show',
		    [
			    'category' => $item,
		    ]
	    );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
	    return view('admin.categories.create',
		    [
			    'category' => Categories::find($id),
			    'categories' => Categories::all(),
		    ]
	    );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
	    if ($request->parentCat == -1) {
		    $request->request->set('parentCat', null);
	    }
	    Categories::find($id)->update($request->all());

	    return redirect('/admin/categories');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
	    Categories::find($id)->delete();

	    return redirect('/admin/categories');
    }
}
