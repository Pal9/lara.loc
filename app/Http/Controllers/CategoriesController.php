<?php

namespace App\Http\Controllers;

use App\models\admin\Categories;
use Illuminate\Http\Request;

class CategoriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
	    return view('categories.index', [
		    'categories' => Categories::where('parentCat', '=', NULL)->paginate(9),
		    'breadcrumbs' => Categories::getBreadcrumbs(),
	    ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\models\admin\Categories  $category
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    	$category = Categories::find($id);
        return view('categories.show', [
        	'category' => $category,
        	'categories' => $category->categories,
        	'goods' => $category->goods->paginate(6),
	        'breadcrumbs' => Categories::getBreadcrumbs($category),
        ]);
    }

    public function map() {
    	return view('categories.map', [
    		'root_categories' => Categories::where('parentCat', '=', NULL)->get(), // в структуре сайта для корректной его работы должны быть _хотя бы одна_ корневая категория
	    ]);
    }
}
