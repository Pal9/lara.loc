<?php

namespace App\Http\Controllers;

use App\Mail\OrdersMailer;
use App\models\admin\Goods;
use App\models\CartInterface;
use App\models\Orders;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class OrdersController extends Controller
{

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
	    $user = \Auth::user();
	    return view('orders/create',
		    [
			    'emailPreset' => isset($user) ? $user->email : '',
		    ]
	    );
    }

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param Request $request
	 *
	 * @return \Illuminate\Http\Response
	 * @throws \ErrorException
	 */
    public function store(Request $request)
    {

	    if ($request->worker instanceof CartInterface) {
		    $products = $request->worker->getProductList($request);
	    } else {
		    throw new \ErrorException('Ошибка обработчика корзины.');
	    }

    	$request->request->set('goods', implode(',', $products));

	    // валидация
	    $this->validate($request, [
		    'name' => 'required|string',
		    'address' => 'required|string',
		    'email' => 'required|email',
		    'goods' => 'required|string',
	    ], [
		    'goods.required' => 'В корзине должен быть хотя бы один товар!',
		    'email' => 'Поле :attribute должно содержать настоящий email-адрес!',
		    'required' => 'Поле :attribute обязательно для заполнения!',
		    'integer' => 'Поле :attribute должно содержать только цифры',
	    ]);

	    $order = Orders::create($request->all());

	    if (!empty($request->email)) {
	    	try {
			    Mail::to($request->email)->send(new OrdersMailer($order));
		    } catch (\Exception $exception) {
			    Log::notice('При отправке подтверждения заказа на адрес ' . $request->email . ' произошла ошибка: ' . $exception->getMessage());
		    }
	    }

	    foreach ($products as $prodId) {
		    $request->worker->removeFromCart($prodId);
	    }

	    Cookie::queue('cart', serialize([])); // очищаем корзину гостей и сбрасываем кэш корзины авторизованным

	    return redirect('/orders/' . $order->id);
    }

	/**
	 * Display the specified resource.
	 *
	 * @param Request $request
	 * @param  int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function show(Request $request, int $id)
	{
		$item = Orders::find($id);

		if ($request->get('s') !== md5($item->name . $item->address)) {
			// на страницу можно попасть только с ключом из письма, остальных отправляем на главную
			return redirect('/');
		}

		$prodIds = explode(',', $item->goods);
		$goods = [];

		foreach ( $prodIds as $prod_id ) {
			$goods[] = Goods::find($prod_id);
		}

		return view('orders.show',
			[
				'order' => $item,
				'goods' => $goods,
			]
		);
	}
}
