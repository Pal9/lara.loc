<?php

namespace App\Http\Controllers;

use App\models\admin\Goods;
use Illuminate\Http\Request;

class GoodsController extends Controller
{

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		return view('goods/index', [
			'goods' => Goods::paginate(9),
		]);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
		$item = Goods::find($id);

		return view('goods.show',
			[
				'good' => $item,
				'comments' => $item->comments->sortByDesc('created_at'),
				'breadcrumbs' => Goods::getBreadcrumbs($item),
			]
		);
	}
}
