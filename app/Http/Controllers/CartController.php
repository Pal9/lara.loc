<?php

namespace App\Http\Controllers;

use App\models\CartInterface;
use Illuminate\Http\Request;

class CartController extends Controller
{

	/**
	 * @param Request $request
	 *
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 * @throws \ErrorException
	 */
	public function show(Request $request) {
		if ($request->worker instanceof CartInterface) {
			$products = $request->worker->getProducts($request);
		} else {
			throw new \ErrorException('Ошибка обработчика корзины.');
		}

		return view('cart.show', [
			'products' => $products,
			'rate' => app()->make('Rate', ['currencyId' => 145]),
		]);
	}

	/**
	 * Добавляем товар в корзину
	 *
	 * @param Request $request
	 *
	 * @return $this
	 * @throws \ErrorException
	 */
	public function addProduct(Request $request) {
		if ($request->worker instanceof CartInterface) {
			$cart = $request->worker->addToCart($request);
		} else {
			throw new \ErrorException('Ошибка обработчика корзины.');
		}

		return response()->json([
			$cart,
		])
			->cookie('cart', serialize($cart)); // для гостей основное сохранение, для авторизованных - на случай разлогина
	}

	/**
	 * @param Request $request
	 * @param int $id
	 *
	 * @return $this
	 * @throws \ErrorException
	 */
	public function removeProduct(Request $request) {
		if ($request->worker instanceof CartInterface) {
			$cart = $request->worker->removeFromCart($request->prodId);
		} else {
			throw new \ErrorException('Ошибка обработчика корзины.');
		}

		return redirect('/cart')
			->cookie('cart', serialize($cart));
	}
}
