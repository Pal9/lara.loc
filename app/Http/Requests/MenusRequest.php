<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MenusRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
	public function rules()
	{
		return [
			'name' => 'required',
			'link' => 'required',
			'type' => 'integer|required',
			'icon' => 'nullable|string',
		];
	}

	public function messages() {
		return [
			'required' => 'Поле :attribute обязательно для заполнения!',
			'integer' => 'Поле :attribute должно содержать только цифры',
			'string' => 'Поле :attribute должно содержать строку',
		];
	}
}
