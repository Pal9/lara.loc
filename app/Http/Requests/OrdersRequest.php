<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class OrdersRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
	public function rules()
	{
		return [
			'name' => 'required|string',
			'address' => 'required|string',
			'email' => 'required|email',
		];
	}

	public function messages() {
		return [
			'email' => 'Поле :attribute должно содержать настоящий email-адрес!',
			'required' => 'Поле :attribute обязательно для заполнения!',
			'integer' => 'Поле :attribute должно содержать только цифры',
		];
	}
}
