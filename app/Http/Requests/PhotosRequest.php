<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PhotosRequest extends FormRequest
{
	public $path;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
	        'name' => 'required',
	        'alt' => 'required',
	        'title' => 'required',
	        'image' => 'file',
	        'path' => '',
        ];
    }

    public function messages() {
	    return [
	    	'required' => 'Поле :attribute обязательно для заполнения!',
	    ];
    }
}
