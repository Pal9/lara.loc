<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class checkAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
    	if (Auth::user() !== null
	        && Auth::user()->email === 'pal9yni4bi@gmail.com') {

//    		$request->myVeryCustomField = new \stdClass();
//    		$request->myVeryCustomField->name = 'Uasya';
//    		$request->myVeryCustomField->surname = 'Pupkin';

		    return $next($request);
	    }

	    Log::notice((Auth::user() === null ? 'Злобный анонимус' : Auth::user()->name) . ' пытался зайти в админку!');

	    return redirect('/login');
    }
}
