<?php

namespace App\Http\Middleware;

use App\models\CartAuth;
use App\models\CartGuest;
use Closure;
use Illuminate\Support\Facades\Auth;

class PrepareCart
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
	    $request->worker = Auth::user() === null ? new CartGuest : new CartAuth;

        return $next($request);
    }
}
