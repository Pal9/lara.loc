<?php

namespace App\Providers;

use Illuminate\Contracts\Container\Container;
use Illuminate\Support\ServiceProvider;

class CurrencyProvider extends ServiceProvider
{

	public const RUR_ID = 141; // российский рубль
	public const USD_ID = 145; // доллар США
	public const EUR_ID = 292; // Евро

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('Rate', function (Container $container, array $params) {

        	if (isset($params['currencyId']) && is_numeric($params['currencyId'])) {
        		$currencyId = $params['currencyId']; // в параметрах можно передать требуемый курс валюты
	        } else {
		        $currencyId = self::USD_ID; // по умолчанию возвращаем курс USD
	        }

	        $ctx = stream_context_create([
	        	'http' => [
			        'timeout' => 1,  // щедро даём 1 секунду на забор данных от АПИ
		        ]
	        ]);

        	$info = json_decode(file_get_contents('http://www.nbrb.by/API/ExRates/Rates/' . $currencyId,false, $ctx));

        	if ($info->Cur_OfficialRate === null) {
        		return 1; // если что-то пошло не так, вернём курс 1 к 1
	        }

        	return $info->Cur_OfficialRate;
        });
    }
}
