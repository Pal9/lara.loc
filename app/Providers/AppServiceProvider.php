<?php

namespace App\Providers;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
	    if (!Collection::hasMacro('paginate')) {

		    Collection::macro('paginate',
			    function ($perPage = 15, $page = null, $options = []) {
				    $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);
				    return (new LengthAwarePaginator(
					    $this->forPage($page, $perPage), $this->count(), $perPage, $page, $options))
					    ->withPath('');
			    });
	    }

	    $collection = collect([
	    	['name' => 'orders', 'link' => ''],
	    	['name' => 'goods', 'link' => '/admin/goods'],
	    ]);

	    view()->composer('includes.menu', function($view) use ($collection) {
	    	$view->with('collection', $collection->all());
	    });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
