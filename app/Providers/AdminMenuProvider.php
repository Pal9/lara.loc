<?php

namespace App\Providers;

use App\models\Menus;
use Illuminate\Support\ServiceProvider;

class AdminMenuProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
	    view()->composer('includes.admin_menu', function($view) {
		    $view->with('menu', Menus::getItems());
	    });
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
