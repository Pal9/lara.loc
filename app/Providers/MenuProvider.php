<?php

namespace App\Providers;

use App\models\Menus;
use Illuminate\Support\ServiceProvider;

class MenuProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
	    view()->composer('includes.menu', function($view) {
		    $view->with('menu', Menus::getItems(2));
	    });
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
