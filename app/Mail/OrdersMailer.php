<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class OrdersMailer extends Mailable
{
    use Queueable, SerializesModels;

    private $orderInfo;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($orderInfo)
    {
	    $this->orderInfo = $orderInfo;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('orders.order-confirm', [
        	'orderInfo' => $this->orderInfo,
        ]);
    }
}
