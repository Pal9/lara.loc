@extends('layouts.client')

@section('content')
<div class="container">

    <div class="content">
        <h1>{{ config('app.name', 'Laravel') }}</h1>
        <div class="row">
            <div class="col-12 col-md-2">
                <ul class="list-unstyled" style="background: #333; padding: 1em 0.3em; border-radius: 0.5em;">
                    @include('includes.menu')
                </ul>
            </div>
            <div class="col-12 col-md-10">
                <p>Учебно-иронический проект "{{ config('app.name', 'Laravel') }}" рад продемонстрировать и подтвердить некоторые навыки из числа полученных в ходе изучения курса "PHP Frameworks. Профессиональная разработка для WEB. Laravel".</p>
                <p>Содержимое сайта не претендует на абсолютную адекватность, а удобство не идеально, но достаточно, чтобы справиться с поставленной целью.</p>
            </div>
        </div>
    </div>
</div>
@endsection