@extends('layouts.admin')

@section('content')
    <div class="container">
        <div class="row">
            <h1>Просмотр адреса</h1>
        </div>
        <div class="row">
            <div class="col-4">Товар</div>
            <div class="col-8">{{$comment->goods->name}}</div>
        </div>
        <div class="row">
            <div class="col-4">Текст</div>
            <div class="col-8">{{$comment->text}}</div>
        </div>
        <a href="/admin/comments/edit/{{$comment->id}}" class="btn btn-primary">Редактировать</a>
    </div>
@endsection()