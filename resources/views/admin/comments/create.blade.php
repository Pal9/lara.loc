@extends('layouts.admin')

@section('content')
    <div class="container">
        <div class="row">
            <h1>{{empty($comment->id) ? 'Новый' : 'Изменить'}} комментарий</h1>
        </div>
        @if (count($errors))
            <div class="alert alert-danger">
                <ul>
                    @foreach($errors->all() as $error)
                        <li>{{$error}}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <form action="/admin/comments{{empty($comment->id) ? '' : '/update/' . $comment->id}}" method="post">
            {{ csrf_field() }}
            {{ empty($comment->id) ? '' : method_field('patch') }}
            <div class="form-group">
                <label for="name">ID товара: </label>
                <input type="text" class="form-control" id="good_id" name="good_id" placeholder="ID товара" value="{{isset ($comment->goods) ? $comment->goods->id : ''}}">
            </div>
            <div class="form-group">
                <label for="alt">Текст комментария</label>
                <input type="text" class="form-control" id="text" name="text" placeholder="Текст комментария" value="{{$comment->text}}">
            </div>
            <button type="submit" class="btn btn-primary">Сохранить</button>
        </form>
        <hr>
        <form action="/admin/comments/{{$comment->id}}" method="post">
            <input type="submit" class="btn btn-danger" value="Удалить" >
            {!! method_field('delete') !!}
            {!! csrf_field() !!}
        </form>
    </div>
@endsection()