@extends('layouts.admin')

@section('title', 'Комментарии')

@section('content')

    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2">@yield('title')</h1>
        <div class="btn-toolbar mb-2 mb-md-0">
            <div class="btn-group mr-2">
                <a href="/admin/comments/create" class="btn btn-sm btn-outline-secondary">Добавить комметнтарий</a>
            </div>
        </div>
    </div>
    <div class="table-responsive">
        <table class="table table-striped table-sm">
            <thead>
            <tr>
                <th>#</th>
                <th>Текст</th>
                <th>Товар</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            @foreach($comments as $comment)
                <tr>
                    <td>{{$comment->id}}</td>
                    <td><a href="/admin/comments/show/{{$comment->id}}">{{$comment->text}}</a></td>
                    <td>{{$comment->goods->name}}</td>
                    <td><a href="/admin/comments/edit/{{$comment->id}}">Редактировать</a></td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection