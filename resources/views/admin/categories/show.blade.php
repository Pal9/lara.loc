@extends('layouts.admin')

@section('content')
    <div class="container">
        <div class="row">
            <h1>Просмотр информации о категории</h1>
        </div>
        <div class="row">
            <div class="col-4">#</div>
            <div class="col-8">{{$category->id}}</div>
        </div>
        <div class="row">
            <div class="col-4">Название</div>
            <div class="col-8">{{$category->title}}</div>
        </div>
        <div class="row">
            <div class="col-4">Родительская категория</div>
            <div class="col-8">{{$category->parentCategory->title}}</div>
        </div>
        <a href="/admin/categories/edit/{{$category->id}}" class="btn btn-primary">Редактировать</a>
    </div>
@endsection()