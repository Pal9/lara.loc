@extends('layouts.admin')

@section('content')
    <div class="container">
        <div class="row">
            <h1>{{empty($category->id) ? 'Новая категория' : 'Изменить категорию'}}</h1>
        </div>
        @if (count($errors))
            <div class="alert alert-danger">
                <ul>
                    @foreach($errors->all() as $error)
                        <li>{{$error}}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <form action="/admin/categories{{ empty($category->id) ? '' : ('/update/' . $category->id) }}" method="post">
            {{ csrf_field() }}
            {{ empty($category->id) ? '' : method_field('patch') }}
            <div class="form-group">
                <label for="title">Название</label>
                <input type="text" class="form-control" id="title" name="title" placeholder="Название" value="{{$category->title}}">
            </div>
            <div class="form-group">
                <div class="form-group">
                    <label for="parentCat">Родительская категория:</label>
                    <select name="parentCat" id="parentCat" class="form-control">
                        <option value="-1">Корневая категория</option>
                        @foreach ($categories as $innerCategory)
                            <option value="{{$innerCategory->id}}" {{$innerCategory->id === $category->parentCat ? 'selected' : ''}}>
                                {{$innerCategory->title}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <button type="submit" class="btn btn-primary">Сохранить</button>
        </form>
        <hr>
        <form action="/admin/categories/{{$category->id}}" method="post">
            <input type="submit" class="btn btn-danger" value="Удалить" >
            {{ method_field('delete') }}
            {{ csrf_field() }}
        </form>
    </div>
@endsection()