@extends('layouts.admin')

@section('title', 'Категории')

@section('content')

    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2">@yield('title')</h1>
        <div class="btn-toolbar mb-2 mb-md-0">
            <div class="btn-group mr-2">
                <a href="/admin/categories/create" class="btn btn-sm btn-outline-secondary">Добавить категорию</a>
            </div>
        </div>
    </div>
    <div class="table-responsive">
        <table class="table table-striped table-sm">
            <thead>
            <tr>
                <th>#</th>
                <th># родителя</th>
                <th>Название</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            @foreach($categories as $category)
                <tr>
                    <td>{{$category->id}}</td>
                    <td>{{$category->parentCat}}</td>
                    <td><a href="/admin/categories/show/{{$category->id}}">{{$category->title}}</a></td>
                    <td><a href="/admin/categories/edit/{{$category->id}}">Редактировать</a></td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
    <div class="align-content-center">{{$categories->links()}}</div>
@endsection