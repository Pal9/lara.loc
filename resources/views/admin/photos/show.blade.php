@extends('layouts.admin')

@section('content')
    <div class="container">
        <div class="row">
            <h1>Просмотр фото</h1>
        </div>
        <div class="row">
            <div class="col-4">Название</div>
            <div class="col-8">{{$photo->name}}</div>
        </div>
        <div class="row">
            <div class="col-4">Alt</div>
            <div class="col-8">{{$photo->alt}}</div>
        </div>
        <div class="row">
            <div class="col-4">Title</div>
            <div class="col-8">{{$photo->title}}</div>
        </div>
        <div class="row">
            <div class="col-4">Path</div>
            <div class="col-8">{{$photo->path}}</div>
        </div>
        <div class="row">
            <div class="col-12"><img src="{{$photo->path}}" alt=""></div>
        </div>
        <a href="/admin/photos/edit/{{$photo->id}}" class="btn btn-primary">Редактировать</a>
    </div>
@endsection()