@extends('layouts.admin')

@section('title', 'Фото')

@section('content')

    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2">@yield('title')</h1>
        <div class="btn-toolbar mb-2 mb-md-0">
            <div class="btn-group mr-2">
                <a href="/admin/photos/create" class="btn btn-sm btn-outline-secondary">Добавить фото</a>
            </div>
        </div>
    </div>
    <div class="table-responsive">
        <table class="table table-striped table-sm">
            <thead>
            <tr>
                <th>#</th>
                <th>Name</th>
                <th>Alt</th>
                <th>Title</th>
                <th>Path</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            @foreach($photos as $photo)
                <tr>
                    <td>{{$photo->id}}</td>
                    <td><a href="/admin/photos/show/{{$photo->id}}">{{$photo->name}}</a></td>
                    <td>{{$photo->alt}}</td>
                    <td>{{$photo->title}}</td>
                    <td>{{$photo->path}}</td>
                    <td><a href="/admin/photos/edit/{{$photo->id}}">Редактировать</a></td>
                </tr>
            @endforeach
            </tbody>
        </table>
        <div class="align-content-center">{{$photos->links()}}</div>
    </div>
@endsection