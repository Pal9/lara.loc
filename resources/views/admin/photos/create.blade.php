@extends('layouts.admin')

@section('content')
    <div class="container">
        <div class="row">
            <h1>{{empty($photo->id) ? 'Новое' : 'Изменение'}} фото</h1>
        </div>
        @if (count($errors))
            <div class="alert alert-danger">
                <ul>
                    @foreach($errors->all() as $error)
                        <li>{{$error}}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <form action="/admin/photos{{empty($photo->id) ? '' : '/update/' . $photo->id}}" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}
            {{ empty($photo->id) ? '' : method_field('patch') }}
            <div class="form-group">
                <label for="name">Название</label>
                <input type="text" class="form-control" id="name" name="name" placeholder="Название" value="{{$photo->name}}">
            </div>
            <div class="form-group">
                <label for="alt">Alt</label>
                <input type="text" class="form-control" id="alt" name="alt" placeholder="Alt" value="{{$photo->alt}}">
            </div>
            <div class="form-group">
                <label for="title">Title</label>
                <input type="text" class="form-control" id="title" name="title" placeholder="Title" value="{{$photo->title}}">
            </div>
            <div class="form-group">
                <label for="image">Фото</label>
                <input type="file" class="form-control" id="image" name="image" placeholder="image">
            </div>
            <img src="{{$photo->path}}">
            <button type="submit" class="btn btn-primary">Сохранить</button>
        </form>
        <hr>
        <form action="/admin/photos/{{$photo->id}}" method="post">
            <input type="submit" class="btn btn-danger" value="Удалить" >
            {!! method_field('delete') !!}
            {!! csrf_field() !!}
        </form>
    </div>
@endsection()