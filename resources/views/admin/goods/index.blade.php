@extends('layouts.admin')

@section('title')
    Товары (найдено {{$count}})
@endsection

@section('content')

    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2">@yield('title')</h1>
        <div class="btn-toolbar mb-2 mb-md-0">
            <div class="btn-group mr-2">
                <a href="/admin/goods/create" class="btn btn-sm btn-outline-secondary">Добавить товар</a>
            </div>
        </div>
    </div>
    <form class="row" method="post" action="{{route('searchGoods')}}">
        {{ csrf_field() }}
        <div class="col-3">
            <div class="form-group">
                <label for="by-string">Содержит строку:</label>
                <input type="text" class="form-control" id="by-string" name="by-string" placeholder="Например, 'тел'" value="{{$searchBy['by-string']}}">
            </div>
        </div>
        <div class="col-3">
            <div class="form-group">
                <label for="price-from">Цена от:</label>
                <input type="number" class="form-control" id="price-from" name="price-from" placeholder="Например, 100" value="{{$searchBy['price-from']}}">
            </div>
        </div>
        <div class="col-3">
            <div class="form-group">
                <label for="price-to">Цена до:</label>
                <input type="number" class="form-control" id="price-to" name="price-to" placeholder="Например, 200" value="{{$searchBy['price-to']}}">
            </div>
        </div>
        <div class="col-3">
            <div class="form-group">
                <br>
                <button type="submit" class="btn btn-primary">Искать</button>
                <a href="{{route('goodsIndex')}}" class="btn btn-danger">Сбросить фильтр</a>
            </div>
        </div>
    </form>
    <div class="table-responsive">
        <table class="table table-striped table-sm">
            <thead>
            <tr>
                <th>#</th>
                <th>Name</th>
                <th>Price</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            @foreach($goods as $good)
                <tr>
                    <td>{{$good->id}}</td>
                    <td><a href="/admin/goods/show/{{$good->id}}">{{$good->name}}</a></td>
                    <td>{{$good->price}}</td>
                    <td><a href="/admin/goods/edit/{{$good->id}}">Редактировать</a></td>
                </tr>
            @endforeach
            </tbody>
        </table>
        <p>Общая цена товаров на странице: {{ $goods->sum('price') }}, цена всех товаров, соответствующих условиям {{$sum}}</p>
        <div class="align-content-center">{{$goods->links()}}</div>
    </div>
@endsection