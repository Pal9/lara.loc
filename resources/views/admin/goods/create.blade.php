@extends('layouts.admin')

@section('content')
    <div class="container">
        <div class="row">
            <h1>{{empty($good->id) ? 'Новый' : 'Изменить'}} товар</h1>
        </div>
        @if (count($errors))
            <div class="alert alert-danger">
                <ul>
                    @foreach($errors->all() as $error)
                        <li>{{$error}}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <form action="/admin/goods{{empty($good->id) ? '' : '/update/' . $good->id}}" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}
            {{ empty($good->id) ? '' : method_field('patch') }}
            <div class="form-group">
                <label for="name">Название</label>
                <input type="text" class="form-control" id="name" name="name" placeholder="Название" value="{{$good->name}}" required>
            </div>
            <div class="form-group">
                <label for="price">Цена, $</label>
                <input type="number" class="form-control" id="price" name="price" placeholder="Например, 100" value="{{$good->price}}" required>
            </div>
            <div class="form-group">
                <label for="alt">Краткое описание</label>
                <input type="text" class="form-control" id="short_description" name="short_description" placeholder="Краткое описание" value="{{$good->short_description}}" required>
            </div>
            <div class="form-group">
                <label for="title">Подробное описание</label>
                <textarea type="text" class="form-control" id="description" name="description" placeholder="Подробное описание" required>{{$good->description}}</textarea>
            </div>
            <div class="form-group">
                <label for="image">Превью товара</label>
                <input type="file" class="form-control" id="image" name="image" placeholder="Загрузите превью товара"
                       value="{{$good->icon}}">
                @if ($good->icon)
                    <img src="{{$good->icon}}" class="w-100">
                @endif
            </div>

            <div class="form-group">
                <div class="form-group">
                    <label for="parentCat">Родительская категория:</label>
                    <select name="parentCat" id="parentCat" class="form-control">
                        <option value="-1">Корневая категория</option>
                        @foreach ($categories as $category)
                            <option value="{{$category->id}}" {{$category->id === $good->parentCat ? 'selected' : ''}}>
                                {{$category->title}}</option>
                        @endforeach
                    </select>
                </div>
            </div>

            <h3>Наличие на складах</h3>
            <div class="form-group">
                @foreach($pickup_points as $point)
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="checkbox" id="pickup_points_{{ $point->id }}" name="pickup_points[]" value="{{ $point->id }}" {{ in_array($point->id, $checked_pickup_points) ? 'checked' : '' }}>
                        <label class="form-check-label" for="pickup_points_{{ $point->id }}">{{ $point->name }}</label>
                    </div>
                @endforeach
            </div>

            <h3><label for="photos">Фото в карусели</label></h3>
            <div class="form-group">
                <select multiple class="form-control" id="photos" name="photos[]">
                    <option value="-1" {{ empty($checked_photos) ? 'selected' : '' }}>Нет фото</option>
                    @foreach($photos as $photo)
                        <option value="{{ $photo->id }}" {{ in_array($photo->id, $checked_photos) ? 'selected' : '' }}>{{ $photo->name }}</option>
                    @endforeach
                </select>
            </div>

            <button type="submit" class="btn btn-primary">Сохранить</button>
        </form>
        <hr>
        <form action="/admin/goods/{{$good->id}}" method="post">
            <input type="submit" class="btn btn-danger" value="Удалить" >
            {!! method_field('delete') !!}
            {!! csrf_field() !!}
        </form>
    </div>
@endsection()