@extends('layouts.admin')

@section('content')
    <div class="container">
        <div class="row">
            <h1 class="col-12">Просмотр товара</h1>
        </div>
        <div class="row">
            <div class="col-4">Название</div>
            <div class="col-8">{{$good->name}}</div>
        </div>
        <div class="row">
            <div class="col-4">Цена</div>
            <div class="col-8">{{$good->price}}</div>
        </div>
        <div class="row">
            <div class="col-4">Краткое описание</div>
            <div class="col-8">{{$good->short_description}}</div>
        </div>
        <div class="row">
            <div class="col-4">Развёрнутое описание</div>
            <div class="col-8">{{$good->description}}</div>
        </div>
        <div class="row">
            <div class="col-4">Превью товара</div>
            <div class="col-8"><img src="{{$good->icon}}" alt="" class="img-fluid"></div>
        </div>
        <a href="/admin/goods/edit/{{$good->id}}" class="btn btn-primary">Редактировать</a>
    </div>

    @foreach($comments as $comment)
        <hr>
        {{$comment->text}}
    @endforeach

    <div class="card-block">
        <form action="/admin/comments" method="post">
            {{ csrf_field() }}
            <input type="hidden" value="{{$good->id}}" name="good_id">
            <div class="form-group">
                <textarea name="text" id="" cols="30" rows="10" placeholder="Оставьте комментарий" class="form-control"></textarea>
            </div>
            <div class="form-group">
                <input type="submit" class="btn btn-primary" value="Сохранить">
            </div>
        </form>
    </div>

    <h2>Фото</h2>
    <hr>
    @foreach($good->photos as $photo)
        <hr>
        {{$photo->path}}
    @endforeach

    <h2>Склады</h2>
    <hr>
    @foreach($good->pickup_points as $point)
        <hr>
        {{$point->name}} : {{$point->address}}
    @endforeach

@endsection()