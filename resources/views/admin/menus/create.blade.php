@extends('layouts.admin')

@section('content')
    <div class="container">
        <div class="row">
            <h1>{{ empty($menuItem->id) ? 'Новый' : 'Изменить' }} пункт меню</h1>
        </div>
        @if (count($errors))
            <div class="alert alert-danger">
                <ul>
                    @foreach($errors->all() as $error)
                        <li>{{$error}}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <form action="/admin/menus{{ empty($menuItem->id) ? '' : ('/update/' . $menuItem->id) }}" method="post">
            {{ csrf_field() }}
            {{ empty($menuItem->id) ? '' : method_field('patch') }}
            <div class="form-group">
                <label for="name">Заголовок меню</label>
                <input type="text" class="form-control" id="name" name="name" placeholder="Например, 'Админка'" value="{{ $menuItem->name }}">
            </div>
            <div class="form-group">
                <label for="link">Адрес ссылки</label>
                <input type="text" class="form-control" id="link" name="link" placeholder="Например, /admin" value="{{ $menuItem->link }}">
            </div>
            <div class="form-group">
                <label for="type">Адрес ссылки</label>
                <input type="number" class="form-control" id="type" name="type" placeholder="1 для админки, 2 для общей части сайта" value="{{ $menuItem->type }}">
            </div>
            <div class="form-group">
                <label for="icon">Иконка (в соответствии с <a href="https://feathericons.com/" target="_blank">библиотекой Feather</a>)</label>
                <input type="text" class="form-control" id="icon" name="icon" placeholder="Например, list" value="{{ $menuItem->icon }}">
            </div>
            <button type="submit" class="btn btn-primary">Сохранить</button>
        </form>
        <hr>
        <form action="{{ route('deleteMenuItem', [
            'id' => $menuItem->id,
        ]) }}" method="post">
            <input type="submit" class="btn btn-danger" value="Удалить" >
            {{ method_field('delete') }}
            {{ csrf_field() }}
        </form>
    </div>
@endsection()