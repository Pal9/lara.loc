@extends('layouts.admin')

@section('title', 'Меню')

@section('content')

    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2">@yield('title')</h1>
        <div class="btn-toolbar mb-2 mb-md-0">
            <div class="btn-group mr-2">
                <a href="{{ route('createMenuItem') }}" class="btn btn-sm btn-outline-secondary">Добавить пункт меню</a>
            </div>
        </div>
    </div>
    <div class="row">
        <p class="col-12">Тип 1 - меню в админке, тип 2 - меню в общедоступной части сайта.</p>
    </div>
    <div class="table-responsive">
        <table class="table table-striped table-sm">
            <thead>
            <tr>
                <th>#</th>
                <th>Родительское меню (тип)</th>
                <th>Название сылки</th>
                <th>Адрес ссылки</th>
                <td></td>
            </tr>
            </thead>
            <tbody>
            @foreach($menus as $menu)
                <tr>
                    <td>{{$menu->id}}</td>
                    <td>{{$menu->type}}</td>
                    <td><a href="{{ route('showMenuItem', [
                        'id' => $menu->id
                    ]) }}">{{$menu->name}}</a></td>
                    <td><a href="{{ route('showMenuItem', [
                        'id' => $menu->id
                    ]) }}">{{$menu->link}}</a></td>
                    <td><a href="{{ route('editMenuItem', [
                        'id' => $menu->id
                    ]) }}">Редактировать</a></td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
    <div class="align-content-center">{{$menus->links()}}</div>
@endsection