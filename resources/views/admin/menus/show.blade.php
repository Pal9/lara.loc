@extends('layouts.admin')

@section('content')
    <div class="container">
        <div class="row">
            <h1>Просмотр пункта меню</h1>
        </div>
        <div class="row">
            <div class="col-4">#</div>
            <div class="col-8">{{ $menuItem->id }}</div>
        </div>
        <div class="row">
            <div class="col-4">Группировка/тип/родитель</div>
            <div class="col-8">{{ $menuItem->type }}</div>
        </div>
        <div class="row">
            <div class="col-4">Текст ссылки</div>
            <div class="col-8">{{ $menuItem->name }}</div>
        </div>
        <div class="row">
            <div class="col-4">Адрес ссылки</div>
            <div class="col-8">{{ $menuItem->link }}</div>
        </div>
        <div class="row">
            <div class="col-4">Иконка</div>
            <div class="col-8">{{ $menuItem->icon }}</div>
        </div>
        <a href="{{ route('editMenuItem', [
            'id' => $menuItem->id,
        ]) }}" class="btn btn-primary">Редактировать</a>
    </div>
@endsection()