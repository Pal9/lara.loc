@extends('layouts.admin')

@section('content')
    <div class="container">
        <div class="row">
            <h1>Просмотр адреса</h1>
        </div>
        <div class="row">
            <div class="col-4">Название</div>
            <div class="col-8">{{$pickup_point->name}}</div>
        </div>
        <div class="row">
            <div class="col-4">Адрес</div>
            <div class="col-8">{{$pickup_point->address}}</div>
        </div>
        <a href="/admin/pickup_points/edit/{{$pickup_point->id}}" class="btn btn-primary">Редактировать</a>
    </div>

    <div class="container">
        <div class="row">
            <h2>Товары</h2>
        </div>
        @foreach($pickup_point->goods as $good)
            <hr>
            {{$good->name}}
        @endforeach
    </div>

@endsection()