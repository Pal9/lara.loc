@extends('layouts.admin')

@section('title', 'Адреса')

@section('content')

    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2">@yield('title')</h1>
        <div class="btn-toolbar mb-2 mb-md-0">
            <div class="btn-group mr-2">
                <a href="/admin/pickup_points/create" class="btn btn-sm btn-outline-secondary">Добавить адрес</a>
            </div>
        </div>
    </div>
    <div class="table-responsive">
        <table class="table table-striped table-sm">
            <thead>
            <tr>
                <th>#</th>
                <th>Название</th>
                <th>Адрес</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            @foreach($pickup_points as $pickup_point)
                <tr>
                    <td>{{$pickup_point->id}}</td>
                    <td><a href="/admin/pickup_points/show/{{$pickup_point->id}}">{{$pickup_point->name}}</a></td>
                    <td>{{$pickup_point->address}}</td>
                    <td><a href="/admin/pickup_points/edit/{{$pickup_point->id}}">Редактировать</a></td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection