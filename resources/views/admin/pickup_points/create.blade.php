@extends('layouts.admin')

@section('content')
    <div class="container">
        <div class="row">
            <h1>{{empty($pickup_point->id) ? 'Новый' : 'Изменить'}} адрес</h1>
        </div>
        @if (count($errors))
            <div class="alert alert-danger">
                <ul>
                    @foreach($errors->all() as $error)
                        <li>{{$error}}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <form action="/admin/pickup_points{{empty($pickup_point->id) ? '' : '/update/' . $pickup_point->id}}" method="post">
            {{ csrf_field() }}
            {{ empty($pickup_point->id) ? '' : method_field('patch') }}
            <div class="form-group">
                <label for="name">Название</label>
                <input type="text" class="form-control" id="name" name="name" placeholder="Название" value="{{$pickup_point->name}}" required>
            </div>
            <div class="form-group">
                <label for="alt">Адрес</label>
                <input type="text" class="form-control" id="address" name="address" placeholder="Адрес" value="{{$pickup_point->address}}" required>
            </div>
            <button type="submit" class="btn btn-primary">Сохранить</button>
        </form>
        <hr>
        <form action="/admin/pickup_points/{{$pickup_point->id}}" method="post">
            <input type="submit" class="btn btn-danger" value="Удалить" >
            {!! method_field('delete') !!}
            {!! csrf_field() !!}
        </form>
    </div>
@endsection()