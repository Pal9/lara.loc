@extends('layouts.client')

@section('content')
    <div class="container">
        <div class="row">
            <h1 class="col-12">Корзина</h1>
        </div>
        @if ($products)
            @php ($summaryPrice = 0)
            <table class="table">
                <thead>
                <tr>
                    <th scope="col">Название товара</th>
                    <th scope="col">Цена товара</th>
                    <th scope="col"></th>
                </tr>
                </thead>
                <tbody>
                @foreach($products as $good)
                    @php ($summaryPrice += $good->price)
                    <tr>
                        <td scope="row">{{ $good->name }}</td>
                        <td>{{ $good->price }}$ / {{ round($good->price * $rate, 2) }} руб. </td>
                        <td>
                            <form action="{{ route('removeProduct') }}" method="post">
                                <input type="hidden" name="prodId" class="btn btn-danger" value="{{ $good->id }}" >
                                <input type="submit" class="btn btn-danger" value="Удалить" >
                                {!! method_field('delete') !!}
                                {!! csrf_field() !!}
                            </form>
                        </td>
                    </tr>
                @endforeach
                </tbody>
                <tfoot>
                <tr>
                    <td scope="row">Итого:</td>
                    <td>{{ $summaryPrice }}$ / {{ round($summaryPrice * $rate, 2) }} руб. </td>
                    <td><a href="/order" class="btn btn-success btn-lg">Оформить заказ</a></td>
                </tr>
                </tfoot>
            </table>
        @else
            <div class="row">
                <div class="col-12">В корзину пока не добавлено ни одного товара</div>
            </div>
        @endif
    </div>
@endsection()