@extends('layouts.client')

@section('title', $good->name)

@section('content')
    <div class="container">
        <div class="row">
            <h1 class="col-12">@yield('title')</h1>
        </div>
        <div class="row">
            <div class="col-6">

                @if (count($good->photos) > 0)<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                    <ol class="carousel-indicators">
                        @foreach($good->photos as $key => $photo)
                            <li data-target="#carouselExampleIndicators" data-slide-to="{{$key}}" class="{{$key == 0 ? 'active' : ''}}"></li>
                        @endforeach
                    </ol>
                    <div class="carousel-inner">
                        @foreach($good->photos as $key => $photo)
                            <div class="carousel-item {{$key == 0 ? 'active' : ''}}">
                                <img class="d-block w-100" src="{{$photo->path}}" alt="{{$photo->alt}}" title="{{$photo->title}}">
                            </div>
                        @endforeach
                    </div>
                    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
                @else
                    Фото товара скоро появятся!
                @endif
            </div>
            <div class="col-6">
                <div class="row text-center">
                    <div class="col-12 h3">Цена: <span class="h1"><strong>{{$good->price}}$</strong></span> <button class="btn btn-success btn-buy btn-lg" data-prodid="{{$good->id}}" data-csrf="{{csrf_token()}}">Купить</button></div>
                </div>
                <div class="row">
                    <h3 class="col-12">Наличие на складах:</h3>
                    <div class="col-12">
                        @if(count($good->pickup_points) > 0)
                            @foreach($good->pickup_points as $point)
                                <hr>
                                {{$point->name}} : {{$point->address}}
                            @endforeach
                        @else
                            Товар доступен к покупке только под заказ.
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <h2 class="col-12">Описание товара</h2>
            <p class="col-12">{{$good->description}}</p>
        </div>

        <div class="row">
            <h2 class="col-12">Отзывы на {{$good->name}}</h2>
        </div>
        @if ($flash = session('message'))
            <div class="alert alert-success">
                {{$flash}}
            </div>
        @endif

        @foreach($comments as $comment)
            <hr>
            <div class="row">
                <p class="col-8">{{$comment->text}}</p>
                <em class="col-4">(добавлен {{$comment->created_at->format('d-m-Y в H:i:s')}})</em>
            </div>
        @endforeach

        <hr>
        <div class="card-block">
            <form action="/comments" method="post">
                {{ csrf_field() }}
                <input type="hidden" value="{{$good->id}}" name="good_id">
                <div class="form-group">
                    <textarea name="text" id="" cols="30" rows="4" placeholder="Оставьте свой отзыв" class="form-control"></textarea>
                </div>
                <div class="form-group">
                    <input type="submit" class="btn btn-primary" value="Отправить">
                </div>
            </form>
        </div>
    </div>
@endsection()