@extends('layouts.client')

@section('content')

<section class="jumbotron text-center">
    <div class="container">
        <h1 class="jumbotron-heading">Все товары</h1>
    </div>
</section>

<div class="album py-5 bg-light">
    <div class="container">
        <div class="row">
            @foreach($goods as $good)
                <div class="col-md-4">
                    @include('includes.good_in_list')
                </div>
            @endforeach
        </div>
        <div class="align-content-center">{{$goods->links()}}</div>
    </div>
</div>
@endsection