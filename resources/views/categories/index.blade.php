@extends('layouts.client')

@section('content')

<section class="jumbotron text-center">
    <div class="container">
        <h1 class="jumbotron-heading">Основные категории</h1>
    </div>
</section>

<div class="album py-5 bg-light">
    <div class="container">
        <div class="row">
            @foreach($categories as $category)
                <div class="col-md-4">
                    <div class="card mb-4 box-shadow">
                        <div class="card-body">
                            <p class="card-text">{{$category->title}}</p>
                            <div class="d-flex justify-content-between align-items-center">
                                <div class="btn-group">
                                    <a href="{{ route('showCategoryGuest', [
                                        'id' => $category->id,
                                    ]) }}" class="btn btn-sm btn-outline-secondary">Посмотреть</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
        <div class="align-content-center">{{$categories->links()}}</div>
    </div>
</div>
@endsection