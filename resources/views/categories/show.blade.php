@extends('layouts.client')

@section('content')
<section class="jumbotron text-center">
    <div class="container">
        <h1 class="jumbotron-heading">{{$category->title}}</h1>
    </div>
</section>

<div class="album py-5 bg-light">
    <div class="container">
        @if (count($categories))
            <div class="row">
                <h2 class="col-12">Подкатегории</h2>
            </div>
            <div class="row">
                @foreach($categories as $category)
                    <div class="col-md-3">
                        <div class="card mb-4 box-shadow">
                            <div class="card-body">
                                <p class="card-text">{{$category->title}}</p>
                                <div class="d-flex justify-content-between align-items-center">
                                    <div class="btn-group">
                                        <a href="{{ route('showCategoryGuest', [
                                            'id' => $category->id,
                                        ]) }}" class="btn btn-sm btn-outline-secondary">Посмотреть</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        @endif
        @if (count($goods))
            <div class="row">
                <h2 class="col-12">Товары в выбранной категории</h2>
            </div>
            <div class="row">
                @foreach($goods as $good)
                    <div class="col-md-4">
                        @include('includes.good_in_list')
                    </div>
                @endforeach
            </div>
        @endif
        <div class="align-content-center">{{$goods->links()}}</div>
    </div>
</div>
@endsection