<?php
/**
 * @var \Illuminate\Database\Eloquent\Collection $root_categories
 * @param int $padding
 */
function recursiveOutput(\Illuminate\Database\Eloquent\Collection $categories) : void {
	echo '<ul>';
	$categories->each(function ($item, $key) {
		echo '<li>';
		echo '<a href="' . route('showCategoryGuest', [
				'id' => $item->id,
			]) . '">' . $item->title . '</a>';

		recursiveOutput($item->categories);
		echo '</li>';
	});
	echo '</ul>';
}

//dump($root_categories->getItems());
?>

@extends('layouts.client')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-12">
                @if($root_categories)
		            <?php recursiveOutput($root_categories) ?>
                @else
                    Не выбрана ни одна корневая директория
                @endif
            </div>
        </div>
    </div>
@endsection
