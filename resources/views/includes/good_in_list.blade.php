<div class="card mb-4 box-shadow">
    <img class="card-img-top" src="{{$good->icon}}" alt="{{$good->name}}">
    <div class="card-body">
        <p class="card-text">{{$good->name}}</p>
        <p class="card-text">{{$good->short_description}}</p>
        <div class="d-flex justify-content-between align-items-center">
            <div class="btn-group">
                <a href="{{ route('showGoodGuest', [
                                        'id' => $good->id,
                                    ]) }}" class="btn btn-sm btn-outline-secondary">Посмотреть</a>
                <span class="btn btn-sm btn-success btn-buy" data-prodid="{{$good->id}}" data-csrf="{{csrf_token()}}">Купить</span>
            </div>
        </div>
    </div>
</div>