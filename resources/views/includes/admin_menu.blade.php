<?php
/**
 * Created by PhpStorm.
 * User: Андрусь
 * Date: 28.07.18
 * Time: 12:55
 */
?>
@foreach ($menu as $menuItem)
    <li class="nav-item">
        <a class="nav-link {{ $menuItem->link === parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH) ? 'active' : '' }}"
           href="{{ $menuItem->link }}">
            <span data-feather="{{ $menuItem->icon }}"></span>
            {{ $menuItem->name }}
        </a>
    </li>
@endforeach