<?php
/**
 * Created by PhpStorm.
 * User: Андрусь
 * Date: 28.07.18
 * Time: 12:55
 */
?>
@foreach ($menu as $menuItem)
    <li><a href="{{ $menuItem->link }}" class="text-white">{{ $menuItem->name }}</a></li>
@endforeach

@if (Route::has('login'))
    @auth
        <li>
            <a class="text-white" href="{{ route('logout') }}"
               onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                Выйти
            </a>

            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                @csrf
            </form>
        </li>
    @else
        <li><a href="{{ route('login') }}" class="text-white">Войти</a></li>
        <li><a href="{{ route('register') }}" class="text-white">Зарегистрироваться</a></li>
    @endauth
@endif

@if (Auth::user() !== null && Auth::user()->email === 'pal9yni4bi@gmail.com')
    <li><a class="text-white" href="/admin">Админка</a></li>
@endif