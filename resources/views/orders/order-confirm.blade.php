@component('mail::message')
# Ваш новый заказ!

Имя покупателя: {{$orderInfo->name}}<br />
Адрес доставки: {{$orderInfo->address}}

@component('mail::button', ['url' => 'http://lara.loc/orders/' . $orderInfo->id . '?s=' . md5($orderInfo->name . $orderInfo->address)])
Посмотреть заказ на сайте
@endcomponent

Рады видеть Вас снова,<br>
{{ config('app.name') }}
@endcomponent
