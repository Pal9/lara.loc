@extends('layouts.client')

@section('title', 'Подтверждение заказа')

@section('content')

    <div class="container">
        <div class="row">
            <h1 class="col-12">@yield('title')</h1>
        </div>
        @if (count($errors))
            <div class="alert alert-danger">
                <ul>
                    @foreach($errors->all() as $error)
                        <li>{{$error}}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <form action="/order" method="post">
            {{ csrf_field() }}
            <div class="form-group">
                <label for="name">Имя</label>
                <input type="text" class="form-control" id="name" name="name" placeholder="Пожалуйста, представьтесь" required>
            </div>
            <div class="form-group">
                <label for="address">Адрес доставки</label>
                <input type="text" class="form-control" id="address" name="address" placeholder="Куда доставить товар" required>
            </div>
            <div class="form-group">
                <label for="email">E-mail для письма о заказе</label>
                <input type="text" class="form-control" id="email" name="email" placeholder="Куда выслать письмо с подтвержденем заказа?" value="{{ $emailPreset }}" required>
            </div>
            <button type="submit" class="btn btn-primary">Подтвердить заказ</button>
        </form>
    </div>

@endsection()