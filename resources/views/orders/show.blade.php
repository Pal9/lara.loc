@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row">
            <h1 class="col-12">Информация о заказе №{{ $order->id }}</h1>
        </div>
        <div class="row">
            <h2 class="col-12">Контактные данные</h2>
        </div>
        <div class="row">
            <div class="col-4">Имя: {{ $order->name }}</div>
            <div class="col-4">Адрес: {{ $order->address }}</div>
            <div class="col-4">Почта: {{ $order->email }}</div>
        </div>
        <div class="row">
            <h2 class="col-12">Товары в заказе</h2>
        </div>
        <table class="table">
            <thead>
            <tr>
                <th scope="col">Название товара</th>
                <th scope="col">Цена товара</th>
            </tr>
            </thead>
            <tbody>
            @php ($summaryPrice = 0)
            @foreach($goods as $good)
                @php ($summaryPrice += $good->price)
                <tr>
                    <td scope="row">{{ $good->name }}</td>
                    <td>{{ $good->price }}$</td>
                </tr>
            @endforeach
            </tbody>
            <tfoot>
            <tr>
                <td scope="row">Итого:</td>
                <td>{{ $summaryPrice }}$</td>
            </tr>
            </tfoot>
        </table>
    </div>

@endsection()